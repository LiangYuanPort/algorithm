package com.ryan.util;

public class BasicSolution {
    public void swap(int[] nums, int i, int j) {
        int tmp = nums[i];
        nums[i] = nums[j];
        nums[j] = tmp;
    }

    public int getMaxValue(int[] nums) {
        int max = Integer.MIN_VALUE;
        for (int i : nums) {
            if (max<i) max=i;
        }
        return max;
    }
    
    public void printArray(int[] nums) {
        for (int i : nums) {
            System.out.print(i+",");
        }
        System.out.println("");
    }

}
