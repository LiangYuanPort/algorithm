import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import com.ryan.util.*;

public class AppTest {
    private void printArray(int[] nums) {
        for (int i : nums) {
            System.out.print(i+",");
        }
        System.out.println("");
    }
    
    private boolean verifyArray(int [] nums) {
        System.out.print("Verifying ");
        printArray(nums);
        for (int i=1; i<nums.length; i++) {
            if (nums[i]<nums[i-1]) {
                return false;
            }
        }
        return true;
    }

    @Test
    public void testAll() {
        System.out.println("Test started");
        int [][] numsArray = {
            {1,5,2,4,3,0,9,8,6,7}, 
            {},
            {0},
            {0,1,2,3,4,5,6,7,8,9},
            {9,8,7,6,5,4,3,2,1,0}
        };
        Solution obj = new Solution();
        for (int[] nums : numsArray) {
            Assert.assertEquals(true, verifyArray(obj.shellSort(nums)));
            Assert.assertEquals(true, verifyArray(obj.quickSort(nums)));
            Assert.assertEquals(true, verifyArray(obj.mergeSort(nums)));
        }
        System.out.println("Test ended");
    }
}
