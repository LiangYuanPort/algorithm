package com.ryan.util;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Solution extends BasicSolution {

    public int[] shellSort(int[] nums) {
        if (nums==null) return null;
        int groups = 3;
        int gap = 1;
        int[] arr = Arrays.copyOf(nums, nums.length);

        while (gap<nums.length) {
            gap = gap*groups + 1;
        }

        while (gap>0) {
            for (int i=gap; i<arr.length; i++) {
                int tmp = arr[i];
                int j = i - gap;
                while (j>=0 && arr[j]>tmp) {
                    arr[j+gap] = arr[j];
                    j -= gap;
                }
                arr[j+gap] = tmp;
            }
            gap = (int)Math.floor(gap/groups);
        }

        return arr;
    }

    public int[] quickSort(int[] nums) {
        if (nums==null || nums.length<=1) return nums;
        int[] ret = Arrays.copyOf(nums, nums.length);
        quickSortHelper(ret, 0, ret.length-1);
        return ret;
    }

    private void quickSortHelper(int[] nums, int lo, int hi) {
        if (lo>=hi) return;
        int p = nums[lo];
        int i = lo;
        int j = hi;
        while (i < j) {
            while (i<j && nums[j]>=p) j--;
            while (i<j && nums[i]<=p) i++;
            swap(nums, i, j);
        }
        nums[lo] = nums[i];
        nums[i] = p;
        quickSortHelper(nums, lo, j-1);
        quickSortHelper(nums, j+1, hi);
    }

    public int[] mergeSort(int[] num) {
        if (num==null || num.length<2) return num;
        int[] ret = Arrays.copyOf(num, num.length);
        mergeSortHelper(ret);
        return ret;
    }

    private void mergeSortHelper(int[] num) {
        int len = num.length;
        if (len<2) return;
        int mid = len/2;
        int[] a = new int[mid];
        int[] b = new int[len-mid];
        for (int i=0; i<mid; i++) {
            a[i]=num[i];
        }
        for (int i=mid; i<len; i++) {
            b[i-mid]=num[i];
        }
        mergeSortHelper(a);
        mergeSortHelper(b);
        mergeData(num, a, b);
    }

    private void mergeData(int[] num, int[] a, int[] b) {
        int i=0;
        int j=0;
        int k=0;
        while (i<a.length && j<b.length) {
            if (a[i]<b[j]) {
                num[k++] = a[i++];
            }
            else {
                num[k++] = b[j++];
            }
        }
        while (i<a.length) {
            num[k++] = a[i++];
        }
        while (j<b.length) {
            num[k++] = b[j++];
        }
    }

    public static Future<int[]> asyncSort(int[] nums) throws InterruptedException {
        CompletableFuture<int[]> completableFuture = new CompletableFuture<>();
        Executors.newCachedThreadPool().submit(() -> {
            Thread.sleep(5000);
            Solution obj = new Solution();
            int[] ret = obj.quickSort(nums);
            completableFuture.complete(ret);
            return null;    
        });
        return completableFuture;
    }


    public static void main(String[] args) {

        int [] nums = {1,5,2,4,3,0,9,8,6,7};
        Solution obj = new Solution();
        System.out.println("Shell sort");
        int [] ret = obj.shellSort(nums);
        obj.printArray(ret);

        System.out.println("Quick sort");
        ret = obj.quickSort(nums);
        obj.printArray(ret);

        System.out.println("Merge sort");
        ret = obj.mergeSort(nums);
        obj.printArray(ret);

        System.out.println("Async sort and blocked!");
        try {
            Future<int[]> compleFuture = asyncSort(nums);
            ret = compleFuture.get();
            obj.printArray(ret);
        }
        catch(Exception e) {
            e.printStackTrace();
        }

        System.out.println("All done");

    }
}
